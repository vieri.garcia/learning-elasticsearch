# Pasos para levantar la aplicación

Construir la imagen flask_app
- docker-compose build

Levantar los servicios de elastic search y flask_app
- docker-compose up

# Pasos para creación del índice
Abrir otra terminal y ejecutar lo siguiente:

Conexión al contenedor flask_app
- docker exec -it flask_app bash

Creación del índice productos
- curl -X PUT "http://es01:9200/productos/" -H "Content-Type: application/json" -d @code/data/index-mapping.json

Cargado de registros ded producto
- curl -X POST 'http://es01:9200/productos/_bulk?pretty' -H "Content-Type: application/x-ndjson" --data-binary @code/data/productos.json

# En caso de problemas al levantar elastic search ejecutar lo siguiente
- sudo sysctl -w vm.max_map_count=262144 
