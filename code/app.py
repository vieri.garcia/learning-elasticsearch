# Python libraries
from functools import reduce 

# Flask libraries
from flask import Flask, jsonify, request

# Elastic search libraries
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q

# Local libraries
from utils import responseToSourceList

es = Elasticsearch(['es01'],scheme="http",port=9200)
searcher = Search(using=es)
app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello world'

@app.route('/api/v1/producto/', methods=['GET'])
def getProductos():
    response= es.search(index='productos')
    results=responseToSourceList(response)
    return jsonify(results)

@app.route('/api/v1/producto/cantidad/<operador>/<cantidad>', methods=['GET'])
def getProductosByCantidad(operador,cantidad):
    valid_operadores=['gt','gte','lt','lte','eq']
    if operador in valid_operadores:
        if operador == 'eq':
            response=searcher.filter('range', **{'cantidad':{'gte':cantidad,'lte':cantidad}}).execute()
        else:
            response=searcher.filter('range', **{'cantidad':{operador: cantidad}}).execute()
        results=responseToSourceList(response.to_dict())
    else:
        results={"mensaje":"Operador no válido"}
    return jsonify(results)

@app.route('/api/v1/producto/buscar/<texto>', methods=['GET'])
def getProductosByTexto(texto):
    try:
        queries=[(lambda x: Q("wildcard", **{'nombre':{'value':'*'+x+'*'}}))(word) for word in texto.split()]
        query= reduce((lambda x,y:x & y),queries)
        response=searcher.filter(query).execute()
        results=responseToSourceList(response.to_dict())
    except Exception as e:
        results={"mensaje":"Entrada de texto no válida"}
    return jsonify(results)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)

