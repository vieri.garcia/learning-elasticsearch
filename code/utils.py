def addIndexToSource(e):
    e['_source']['id']=e["_id"]
    return e['_source']

def responseToSourceList(response):
    return [addIndexToSource(e) for e in response['hits']['hits']]